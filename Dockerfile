FROM armhf/alpine

RUN apk update && \
    apk --no-cache add \
        libusb-dev \
	libusb \
	libusb-compat-dev \
	make \
	gcc \
        curl \
	musl-dev && \
    mkdir /tmp/pcsensor && \
    curl -L http://raw.githubusercontent.com/padelt/pcsensor-temper/master/src/pcsensor.c -o /tmp/pcsensor/pcsensor.c && \
    curl -L http://raw.githubusercontent.com/padelt/pcsensor-temper/master/src/Makefile -o /tmp/pcsensor/Makefile && \
    cd /tmp/pcsensor/ && \
    make && \
    cp ./pcsensor /usr/local/bin/ && \
    rm -rf /tmp/pcsensor && rm -rf /var/cache/apk/* && \
    apk del --no-cache --rdepends \
	make \
	gcc \
	musl-dev

# -c Show only in Celcius
# -l5 show temperature value in every 5 seconds.
#
CMD ["/usr/local/bin/pcsensor", "-c", "-l5"]
