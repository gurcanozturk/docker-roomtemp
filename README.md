Temperature monitoring by a USB Microdia TEMPer Temperature Sensor runs on Alpine Linux docker container on Raspberry Pi.

Build it.

docker build -t docker-roomtemp .

Run it. (get your USB device address by "lsusb" command)

docker run --rm -it --device=/dev/bus/usb/001/006 docker-roomtemp